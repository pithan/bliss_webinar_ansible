echo -e "\n------------------ bliss install script  ------------------"
cd /bliss/
git checkout user_training
source /opt/conda/bin/activate base
conda config --add channels esrf-bcu
conda config --append channels conda-forge
conda config --append channels tango-controls
conda create --quiet --name demoenv pymca --file requirements-conda.txt --file requirements-test-conda.txt -y >> /install_bliss.log
conda clean -a -y 
#conda activate demoenv
source /opt/conda/bin/activate demoenv
pip install --no-deps -e .
